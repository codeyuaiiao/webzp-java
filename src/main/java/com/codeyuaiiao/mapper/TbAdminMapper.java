package com.codeyuaiiao.mapper;

import com.codeyuaiiao.entity.TbAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author codeyuaiiao
 * @since 2021-01-29
 */
public interface TbAdminMapper extends BaseMapper<TbAdmin> {


}
