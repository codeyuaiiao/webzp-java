package com.codeyuaiiao.mapper;

import com.codeyuaiiao.entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author codeyuaiiao
 * @since 2021-01-29
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}
