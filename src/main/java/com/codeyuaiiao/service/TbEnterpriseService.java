package com.codeyuaiiao.service;

import com.codeyuaiiao.entity.TbEnterprise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author codeyuaiiao
 * @since 2021-01-29
 */
public interface TbEnterpriseService extends IService<TbEnterprise> {

}
