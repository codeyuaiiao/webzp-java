package com.codeyuaiiao.service;

import com.codeyuaiiao.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author codeyuaiiao
 * @since 2021-01-29
 */
public interface TbUserService extends IService<TbUser> {

}
